﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LecturaEscruturaArchivos
{
    public class Alumno
    {
        public string Matricula { get; set; }
        public string Nombre { get; set; }
        public override string ToString()
        {
            return Matricula + "\t" + Nombre;
        }
    }
}
