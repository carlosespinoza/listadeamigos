﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Windows;

namespace LecturaEscruturaArchivos
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        ManejoArchivos manejador;
        List<Alumno> alumnos;
        public MainWindow()
        {
            InitializeComponent();
            manejador = new ManejoArchivos();
            alumnos = new List<Alumno>();
        }

        private void btnSeleccionarArchivo_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dialogo = new OpenFileDialog();
            dialogo.Title = "Selecciona tu archivo";
            dialogo.ShowDialog();
            txbArchivo.Text = dialogo.FileName;
        }

        private void btnAbrir_Click(object sender, RoutedEventArgs e)
        {
            txbContenido.Text = manejador.AbrirArchivo(txbArchivo.Text);
            //separar por lineas
            string[] alumns = txbContenido.Text.Split('\n');
            //recorrer las lineas
            foreach (var alu in alumns)
            {
                //crear un alumno por cada linea
                Alumno a = new Alumno();
                //Separan los datos en la linea
                string[] datos = alu.Split('\t');
                if (datos.Length == 2)
                {
                    a.Matricula =datos[0];
                    a.Nombre = datos[1];
                    alumnos.Add(a);
                }
            }
            CargarLista();
        }

        private void btnGuardar_Click(object sender, RoutedEventArgs e)
        {
            //primero seleccionar el archivo
            SaveFileDialog dialogoSalvar = new SaveFileDialog();
            dialogoSalvar.ShowDialog();
            //guardar el contenido
            if (manejador.GuardarArchivo(txbContenido.Text, dialogoSalvar.FileName))
            {
                MessageBox.Show("Archivo guardado");
            }
            else
            {

                MessageBox.Show("Error al guardar el archivo");
            }
        }

        private void btnAgregar_Click(object sender, RoutedEventArgs e)
        {
            Alumno alumno = new Alumno()
            {
                Matricula = txbMatricula.Text,
                Nombre = txbNombre.Text
            };
            alumnos.Add(alumno);
            txbContenido.Text += alumno.ToString() + "\n";
            CargarLista();
        }

        private void CargarLista()
        {
            lstAlumnos.ItemsSource = null;
            lstAlumnos.ItemsSource = alumnos;
        }

        private void btnMostrarVenta_Click(object sender, RoutedEventArgs e)
        {
            Modelo modelo = new Modelo()
            {
                Alumnos = alumnos,
                DireccionArchivo = txbArchivo.Text,
                FechaHora = DateTime.Now
            };
            //Forma 1, mediante el contructor

            //Ventana2 ventana = new Ventana2(modelo);
            //ventana.Show();

            //forma 2, mediante una propiedad
            //Ventana2 ventana = new Ventana2();
            //ventana.Modelo = modelo;
            //ventana.Show();

            //forma3, mediante una clase estatica
            Parametros.datos = modelo;
            Ventana2 ventana = new Ventana2();
            ventana.Show();
        }
    }
}
