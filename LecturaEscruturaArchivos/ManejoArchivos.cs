﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LecturaEscruturaArchivos
{
    public class ManejoArchivos
    {
        public string AbrirArchivo(string archivo)
        {
            try
            {
                StreamReader archi = new StreamReader(archivo);
                string contenido = archi.ReadToEnd();
                archi.Close();
                return contenido;
            }
            catch (Exception ex)
            {
                return "#########ERROR: " + ex.Message;
            }
            
        }
        public bool GuardarArchivo(string contenido, string archivo)
        {
            try
            {
               
                StreamWriter archi = new StreamWriter(archivo);
                archi.Write(contenido);
                archi.Close();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
            
        }
    }
}
