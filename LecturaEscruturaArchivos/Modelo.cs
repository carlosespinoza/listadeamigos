﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LecturaEscruturaArchivos
{
    public class Modelo
    {
        public List<Alumno> Alumnos { get; set; }
        public string DireccionArchivo { get; set; }
        public DateTime FechaHora { get; set; }
    }
}
