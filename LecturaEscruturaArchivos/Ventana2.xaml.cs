﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace LecturaEscruturaArchivos
{
    /// <summary>
    /// Lógica de interacción para Ventana2.xaml
    /// </summary>
    public partial class Ventana2 : Window
    {
        public Modelo Modelo { get; set; }
        public Ventana2(Modelo modelo)
        {
            InitializeComponent();
            Modelo = modelo;
            
        }

        private void Mensaje()
        {
            MessageBox.Show(String.Format("Ahora son las {0} el archivo es {1} y actualmente hay {2} de registros", Modelo.FechaHora.ToShortTimeString(), Modelo.DireccionArchivo, Modelo.Alumnos.Count), "Aviso", MessageBoxButton.OK, MessageBoxImage.Information);
        }

        public Ventana2()
        {
            InitializeComponent();
            Modelo = Parametros.datos;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Mensaje();
        }
    }
}
