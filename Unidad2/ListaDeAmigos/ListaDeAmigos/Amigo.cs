﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ListaDeAmigos
{
    public class Amigo
    {
        public string Nombre { get; set; }
        public string Apellidos { get; set; }
        public string Apodo { get; set; }
        public string Telefono { get; set; }
        public string Email { get; set; }
    }
}
