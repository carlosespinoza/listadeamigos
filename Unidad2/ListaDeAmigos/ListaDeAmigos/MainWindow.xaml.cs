﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ListaDeAmigos
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        RepositorioDeAmigos repositorio;
        bool esNuevo;
        public MainWindow()
        {
            InitializeComponent();
            repositorio = new RepositorioDeAmigos();
            HabilitarCajas(false);
            HabilitarBotones(true);
            ActualizarTabla();
        }

        private void HabilitarCajas(bool habilitadas)
        {
            txbApellidos.Clear();
            txbApodo.Clear();
            txbEmail.Clear();
            txbNombre.Clear();
            txbTelefono.Clear();
            txbApellidos.IsEnabled = habilitadas;
            txbApodo.IsEnabled = habilitadas;
            txbEmail.IsEnabled = habilitadas;
            txbNombre.IsEnabled = habilitadas;
            txbTelefono.IsEnabled = habilitadas;
        }

        private void HabilitarBotones(bool habilitados)
        {
            btnNuevo.IsEnabled = habilitados;
            btnEditar.IsEnabled = habilitados;
            btnEliminar.IsEnabled = habilitados;
            btnGuardar.IsEnabled = !habilitados;
            btnCancelar.IsEnabled = !habilitados;
        }

        private void btnNuevo_Click(object sender, RoutedEventArgs e)
        {
            HabilitarCajas(true);
            HabilitarBotones(false);
            esNuevo = true;
        }

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            HabilitarCajas(false);
            HabilitarBotones(true);
        }

        private void btnGuardar_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(txbNombre.Text) || string.IsNullOrEmpty(txbApodo.Text) || string.IsNullOrEmpty(txbTelefono.Text))
            {
                MessageBox.Show("Faltan datos", "Error", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }

            if (esNuevo)
            {

                Amigo a = new Amigo()
                {
                    Apellidos = txbApellidos.Text,
                    Apodo = txbApodo.Text,
                    Email = txbEmail.Text,
                    Nombre = txbNombre.Text,
                    Telefono = txbTelefono.Text
                };
                if (repositorio.AgregarAmigo(a))
                {
                    MessageBox.Show("Guardado con Éxito", "Compa", MessageBoxButton.OK, MessageBoxImage.Information);
                    ActualizarTabla();
                    HabilitarBotones(true);
                    HabilitarCajas(false);
                }
                else
                {
                    MessageBox.Show("Error al guardar a tu compa, contactarte a ti mismo ya que tu hiciste este programa", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            else
            {
                Amigo original = dtgTabla.SelectedItem as Amigo;
                Amigo a = new Amigo();
                a.Apellidos = txbApellidos.Text;
                a.Apodo = txbApodo.Text;
                a.Email = txbEmail.Text;
                a.Nombre = txbNombre.Text;
                a.Telefono = txbTelefono.Text;
                if (repositorio.ModificarAmigo(original, a))
                {
                    HabilitarBotones(true);
                    HabilitarCajas(false);
                    ActualizarTabla();
                    MessageBox.Show("Su compa a sido actualizado", "Compas", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                else
                {
                    MessageBox.Show("Error al guardar a tu compa, contactarte a ti mismo ya que tu hiciste este programa", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }
        private void ActualizarTabla()
        {
            dtgTabla.ItemsSource = null;
            dtgTabla.ItemsSource = repositorio.LeerAmigos();
        }

        private void btnEliminar_Click(object sender, RoutedEventArgs e)
        {
            if (repositorio.LeerAmigos().Count == 0)
            {
                MessageBox.Show("Nadie te quiere...", "No tienes amigos", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else
            {
                if (dtgTabla.SelectedItem != null)
                {
                    Amigo a = dtgTabla.SelectedItem as Amigo;
                    if (MessageBox.Show("Realmente deseas eliminar a " + a.Apodo + "?", "Eliminar????", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                    {
                        if (repositorio.EliminarAmigo(a))
                        {
                            MessageBox.Show("Tu Compa ha sido removido", "Compas", MessageBoxButton.OK, MessageBoxImage.Information);
                            ActualizarTabla();
                        }
                        else
                        {
                            MessageBox.Show("Error al eliminar a tu compa, contactarte a ti mismo ya que tu hiciste este programa", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                        }
                    }
                }
                else
                {
                    MessageBox.Show("¿A Quien???", "Compa", MessageBoxButton.OK, MessageBoxImage.Question);
                }
            }
        }

        private void btnEditar_Click(object sender, RoutedEventArgs e)
        {
            if (repositorio.LeerAmigos().Count == 0)
            {
                MessageBox.Show("Nadie te quiere...", "No tienes amigos", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else
            {
                if (dtgTabla.SelectedItem != null)
                {
                    Amigo a = dtgTabla.SelectedItem as Amigo;
                    HabilitarCajas(true);
                    txbApellidos.Text = a.Apellidos;
                    txbApodo.Text = a.Apodo;
                    txbEmail.Text = a.Email;
                    txbNombre.Text = a.Nombre;
                    txbTelefono.Text = a.Telefono;
                    HabilitarBotones(false);
                    esNuevo = false;
                }
                else
                {
                    MessageBox.Show("¿A Quien???", "Compa", MessageBoxButton.OK, MessageBoxImage.Question);
                }
            }
        }
    }
}
