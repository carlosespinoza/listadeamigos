﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ListaDeAmigos
{
    public class RepositorioDeAmigos
    {
        ManejadorDeArchivos archivoAmigos;
        List<Amigo> Amigos;
        public RepositorioDeAmigos()
        {
            archivoAmigos = new ManejadorDeArchivos("MisCompas.poo");
            Amigos = new List<Amigo>();
        }

        public bool AgregarAmigo(Amigo amigo)
        {
            Amigos.Add(amigo);
            bool resultado= ActualizarArchivo();
            Amigos = LeerAmigos();
            return resultado;
        }

        public bool EliminarAmigo(Amigo amigo)
        {
            Amigo temporal = new Amigo();
            foreach (var item in Amigos)
            {
                if (item.Telefono == amigo.Telefono)
                {
                    temporal = item;
                }
            }
            Amigos.Remove(temporal);
            bool resultado = ActualizarArchivo();
            Amigos = LeerAmigos();
            return resultado;
        }

        public bool ModificarAmigo(Amigo original, Amigo modificado)
        {
            Amigo temporal = new Amigo();
            foreach (var item in Amigos)
            {
                if (original.Telefono == item.Telefono)
                {
                    temporal = item;
                }
            }
            temporal.Nombre = modificado.Nombre;
            temporal.Apellidos = modificado.Apellidos;
            temporal.Apodo = modificado.Apodo;
            temporal.Telefono = modificado.Telefono;
            temporal.Email = modificado.Email;
            bool resultado = ActualizarArchivo();
            Amigos = LeerAmigos();
            return resultado;
        }

        private bool ActualizarArchivo()
        {
            string datos = "";
            foreach (Amigo item in Amigos)
            {
                datos += string.Format("{0}|{1}|{2}|{3}|{4}\n", item.Nombre, item.Apellidos, item.Apodo, item.Telefono, item.Email);
            }
            return archivoAmigos.Guardar(datos);
        }
        public List<Amigo> LeerAmigos()
        {
            string datos = archivoAmigos.Leer();
            if (datos != null)
            {
                List<Amigo> amigos = new List<Amigo>();
                string[] lineas = datos.Split('\n');
                for (int i = 0; i < lineas.Length-1; i++)
                {
                    string[] campos = lineas[i].Split('|');
                    Amigo a = new Amigo()
                    {
                        Nombre = campos[0],
                        Apellidos = campos[1],
                        Apodo = campos[2],
                        Telefono = campos[3],
                        Email = campos[4]
                    };
                    amigos.Add(a);
                }
                Amigos = amigos;
                return amigos;
            }
            else
            {
                return null;
            }
        }
    }
}
